package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    private Leilao leilao;
    private Usuario usuario;
    private Lance lance;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        this.leilao = new Leilao();
        this.leiloeiro = new Leiloeiro("João", leilao);
        this.usuario = new Usuario("Lucas", 1);
        this.lance = new Lance(usuario, 250.25);
    }

    @Test
    public void testarIncluirNovoLanceNoLeilao(){
        this.leilao.adicionarNovoLance(this.lance);
        int qtdeLances = this.leilao.getLances().size();

        Assertions.assertEquals(1, qtdeLances);
    }

    @Test
    public void testarRecusaDeNovoLanceComValorMenor(){
        this.leilao.adicionarNovoLance(this.lance);
        Lance lance = new Lance(new Usuario("Victor", 2), 100.00);

        Assertions.assertThrows(RuntimeException.class, () -> {this.leilao.adicionarNovoLance(lance);});
    }

    @Test
    public void testarRetornarMaiorLanceValor(){
        this.leilao.adicionarNovoLance(this.lance);
        Lance lance = new Lance(new Usuario("Lucas", 1), 450.05);
        this.leilao.adicionarNovoLance(lance);

        Assertions.assertEquals(450.05,  this.leiloeiro.getMaiorLanceValor());
    }

    @Test
    public void testarRetornarMaiorLanceUsuario(){
        this.leilao.adicionarNovoLance(this.lance);
        Lance lance = new Lance(new Usuario("Vanessa", 1), 450.05);
        this.leilao.adicionarNovoLance(lance);

        Assertions.assertEquals("Vanessa",  this.leiloeiro.getMaiorLanceUsuario());
    }

    @Test
    public void testarRetornarMaiorLance(){
        this.leilao.adicionarNovoLance(this.lance);
        Lance lance = new Lance(new Usuario("Matheus", 2), 850.00);
        this.leilao.adicionarNovoLance(lance);

        Assertions.assertEquals(lance, this.leiloeiro.getMaiorLance());
    }
}
