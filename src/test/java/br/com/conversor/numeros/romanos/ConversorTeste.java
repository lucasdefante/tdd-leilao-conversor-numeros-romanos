package br.com.conversor.numeros.romanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorTeste {

    @Test
    public void testarConversor1(){
        String resultado = Conversor.converterNaturalParaRomano(1);

        Assertions.assertEquals("I", resultado);
    }

    @Test
    public void testarConversor2(){
        String resultado = Conversor.converterNaturalParaRomano(2);

        Assertions.assertEquals("II", resultado);
    }

    @Test
    public void testarConversor3(){
        String resultado = Conversor.converterNaturalParaRomano(3);

        Assertions.assertEquals("III", resultado);
    }

    @Test
    public void testarConversor4(){
        String resultado = Conversor.converterNaturalParaRomano(4);

        Assertions.assertEquals("IV", resultado);
    }

    @Test
    public void testarConversor5(){
        String resultado = Conversor.converterNaturalParaRomano(5);

        Assertions.assertEquals("V", resultado);
    }

    @Test
    public void testarConversor6(){
        String resultado = Conversor.converterNaturalParaRomano(6);

        Assertions.assertEquals("VI", resultado);
    }

    @Test
    public void testarConversor7(){
        String resultado = Conversor.converterNaturalParaRomano(7);

        Assertions.assertEquals("VII", resultado);
    }

    @Test
    public void testarConversor8(){
        String resultado = Conversor.converterNaturalParaRomano(8);

        Assertions.assertEquals("VIII", resultado);
    }

    @Test
    public void testarConversor9(){
        String resultado = Conversor.converterNaturalParaRomano(9);

        Assertions.assertEquals("IX", resultado);
    }

    @Test
    public void testarConversor10(){
        String resultado = Conversor.converterNaturalParaRomano(10);

        Assertions.assertEquals("X", resultado);
    }

    @Test
    public void testarConversor11(){
        String resultado = Conversor.converterNaturalParaRomano(11);

        Assertions.assertEquals("XI", resultado);
    }

    @Test
    public void testarConversor12(){
        String resultado = Conversor.converterNaturalParaRomano(12);

        Assertions.assertEquals("XII", resultado);
    }

    @Test
    public void testarConversor13(){
        String resultado = Conversor.converterNaturalParaRomano(13);

        Assertions.assertEquals("XIII", resultado);
    }

    @Test
    public void testarConversor14(){
        String resultado = Conversor.converterNaturalParaRomano(14);

        Assertions.assertEquals("XIV", resultado);
    }

    @Test
    public void testarConversor15(){
        String resultado = Conversor.converterNaturalParaRomano(15);

        Assertions.assertEquals("XV", resultado);
    }

    @Test
    public void testarConversor16(){
        String resultado = Conversor.converterNaturalParaRomano(16);

        Assertions.assertEquals("XVI", resultado);
    }

    @Test
    public void testarConversor17(){
        String resultado = Conversor.converterNaturalParaRomano(17);

        Assertions.assertEquals("XVII", resultado);
    }

    @Test
    public void testarConversor18(){
        String resultado = Conversor.converterNaturalParaRomano(18);

        Assertions.assertEquals("XVIII", resultado);
    }

    @Test
    public void testarConversor19(){
        String resultado = Conversor.converterNaturalParaRomano(19);

        Assertions.assertEquals("XIX", resultado);
    }

    @Test
    public void testarConversor20(){
        String resultado = Conversor.converterNaturalParaRomano(20);

        Assertions.assertEquals("XX", resultado);
    }
}
