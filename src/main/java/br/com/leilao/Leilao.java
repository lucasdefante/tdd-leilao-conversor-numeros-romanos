package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances = new ArrayList<>();
    private Lance maiorLance = null;

    public Leilao() {
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance getMaiorLance() {
        return maiorLance;
    }

    public void setMaiorLance(Lance maiorLance) {
        this.maiorLance = maiorLance;
    }

    public void adicionarNovoLance(Lance lance) {
        if(isLanceMaior(lance)){
            this.lances.add(lance);
            this.maiorLance = lance;
        } else {
            throw new RuntimeException("Novo lance deve ser maior ao último lance aceito.");
        }
    }

    public boolean isLanceMaior(Lance lance){
        if(this.maiorLance == null || this.maiorLance.getValorLance() < lance.getValorLance()){
            return true;
        }
        else {
            return false;
        }
    }
}
