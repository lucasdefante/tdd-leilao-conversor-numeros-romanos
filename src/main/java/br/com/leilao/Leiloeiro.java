package br.com.leilao;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public double getMaiorLanceValor() {
        return this.leilao.getMaiorLance().getValorLance();
    }

    public String getMaiorLanceUsuario(){
        return this.leilao.getMaiorLance().getUsuario().getNome();
    }

    public Lance getMaiorLance() { return this.leilao.getMaiorLance(); }
}
