package br.com.conversor.numeros.romanos;

public class Conversor {

    public static String converterNaturalParaRomano(int numero) {
        String resultado = "";
        int condicao = numero;

        while (condicao != 0) {

            if ((condicao % Algarismos.M.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.M.getValor(); i++) {
                    resultado += Algarismos.M.toString();
                    condicao -= Algarismos.M.getValor();
                }
            } else if ((condicao - Algarismos.M.getValor()) >= -100) {
                condicao -= Algarismos.M.getValor();
                resultado += Algarismos.M.toString();
            }

            if ((condicao % Algarismos.D.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.D.getValor(); i++) {
                    resultado += Algarismos.D.toString();
                    condicao -= Algarismos.D.getValor();
                }
            } else if ((condicao - Algarismos.D.getValor()) >= -100) {
                condicao -= Algarismos.D.getValor();
                resultado += Algarismos.D.toString();
            }

            if ((condicao % Algarismos.C.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.C.getValor(); i++) {
                    resultado += Algarismos.C.toString();
                    condicao -= Algarismos.C.getValor();
                }
            } else if ((condicao - Algarismos.C.getValor()) >= -10) {
                condicao -= Algarismos.C.getValor();
                resultado += Algarismos.C.toString();
            }

            if ((condicao % Algarismos.L.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.L.getValor(); i++) {
                    resultado += Algarismos.L.toString();
                    condicao -= Algarismos.L.getValor();
                }
            } else if ((condicao - Algarismos.L.getValor()) >= -10) {
                condicao -= Algarismos.L.getValor();
                resultado += Algarismos.L.toString();
            }

            if ((condicao % Algarismos.X.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.X.getValor(); i++) {
                    resultado += Algarismos.X.toString();
                    condicao -= Algarismos.X.getValor();
                }
            } else if ((condicao - Algarismos.X.getValor()) >= -1) {
                condicao -= Algarismos.X.getValor();
                resultado += Algarismos.X.toString();
            }

            if ((condicao % Algarismos.V.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.V.getValor(); i++) {
                    resultado += Algarismos.V.toString();
                    condicao -= Algarismos.V.getValor();
                }
            } else if ((condicao - Algarismos.V.getValor()) >= -1) {
                condicao -= Algarismos.V.getValor();
                resultado += Algarismos.V.toString();
            }

            if ((condicao % Algarismos.I.getValor()) == 0) {
                for(int i=0; i < condicao / Algarismos.I.getValor(); i++) {
                    resultado += Algarismos.I.toString();
                    condicao -= Algarismos.I.getValor();
                }
            }

            if(condicao < 0){
                condicao *= (-1);
                if ((condicao - Algarismos.M.getValor()) >= 0) {
                    resultado = Algarismos.M.toString() + resultado;
                    condicao -= Algarismos.M.getValor();
                }

                if ((condicao - Algarismos.D.getValor()) >= 0) {
                    resultado = Algarismos.D.toString() + resultado;
                    condicao -= Algarismos.D.getValor();
                }

                if ((condicao - Algarismos.C.getValor()) >= 0) {
                    resultado = Algarismos.C.toString() + resultado;
                    condicao -= Algarismos.C.getValor();
                }

                if ((condicao - Algarismos.L.getValor()) >= 0) {
                    resultado = Algarismos.L.toString() + resultado;
                    condicao -= Algarismos.L.getValor();
                }

                if ((condicao - Algarismos.X.getValor()) >= 0) {
                    resultado = Algarismos.X.toString() + resultado;
                    condicao -= Algarismos.X.getValor();
                }

                if ((condicao - Algarismos.V.getValor()) >= 0) {
                    resultado = Algarismos.V.toString() + resultado;
                    condicao -= Algarismos.V.getValor();
                }

                if ((condicao - Algarismos.I.getValor()) >= 0) {
                    resultado = Algarismos.I.toString() + resultado;
                    condicao -= Algarismos.I.getValor();
                }
            }
        }

        return resultado;
    }
}
